<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EntryForm extends Model
{
	//store the data entered by the user
    public $name;
    public $email;
	
	//returns a set of rules for validating the data
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],						//must be a syntactically valid email address
        ];
    }
}